export interface ListRow {
  type: 'song' | 'talk' | 'jingle' | 'section'
  isSelected: boolean
  isFixed: boolean
  isSpare: boolean
  duration: number
  song?: string
  text?: string
  comment?: string
  bpm?: number
}
export interface ShowDetails {
  id: string
  title: string
  startTime: string
  duration: number
  averageSongLength: number
}

export interface Show {
  meta: ShowDetails
  rows: ListRow[]
}
export interface State {
  isLoading: boolean
  currentSlug: string
  firstTime: boolean
  ui: {
    createRowFormOpen: boolean
    showDetailsFormOpen: boolean
    displaySeconds: boolean
    mode: 'edit' | 'play'
  }
  shows: {
    [index: string]: Show
  }
}
