import { createApp } from 'vue'
import { store, key } from './store'
import { router } from './router'
import App from './App.vue'
import { useLogger } from '@slipmatio/logger/global/vue'
import './assets/tailwind.postcss'

const app = createApp(App)

useLogger()

app.use(store, key)
app.use(router)
app.mount('#app')

// @ts-ignore
if (window.Cypress) {
  // @ts-ignore
  window.vxstore = store
  // @ts-ignore
  console.log('Cypress: Setting store to window: ', window.vxstore)
}
