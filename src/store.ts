/* eslint-disable @typescript-eslint/no-extra-semi */
import { InjectionKey } from 'vue'
import { createStore, Store } from 'vuex'
import { generateUUID, storageAvailable } from '@/composables/helpers'
import { State, ListRow, ShowDetails } from './types'

export const key: InjectionKey<Store<State>> = Symbol()
export const store = createStore<State>({
  state: {
    isLoading: true,
    currentSlug: '',
    firstTime: false,
    ui: {
      createRowFormOpen: false,
      showDetailsFormOpen: false,
      displaySeconds: false,
      mode: 'edit',
    },
    shows: {},
  },
  getters: {
    isReady(state) {
      return !state.isLoading && state.currentSlug
    },

    showDetails(state) {
      if (state.currentSlug) {
        return state.shows[state.currentSlug].meta
      } else {
        return {}
      }
    },

    rows(state) {
      if (state.currentSlug) {
        return state.shows[state.currentSlug].rows
      } else {
        return []
      }
    },

    totalProgramSeconds(state) {
      if (state.currentSlug) {
        const number = state.shows[state.currentSlug].rows.reduce((sum: number, row: ListRow) => {
          return sum + row.duration
        }, 0)
        return number
      } else {
        return 0
      }
    },
    totalProgramMinutes(state, getters) {
      if (state.currentSlug) {
        return Math.round(getters.totalProgramSeconds / 60)
      } else {
        return 0
      }
    },
    emptyProgramSeconds(state, getters) {
      if (state.currentSlug) {
        return state.shows[state.currentSlug].meta.duration * 60 - getters.totalProgramSeconds
      } else {
        return 0
      }
    },
    emptyProgramMinutes(state, getters) {
      if (state.currentSlug) {
        return state.shows[state.currentSlug].meta.duration - getters.totalProgramMinutes
      } else {
        return 0
      }
    },
    neededSongsEstimate(state, getters) {
      if (state.currentSlug) {
        return Math.round(
          getters.emptyProgramSeconds / state.shows[state.currentSlug].meta.averageSongLength
        )
      } else {
        return 0
      }
    },
    currentHomeUrl(state) {
      if (state.currentSlug) {
        return `/${state.currentSlug}/`
      } else {
        return '/'
      }
    },
  },
  mutations: {
    SET_IS_LOADING(state, payload: boolean) {
      state.isLoading = payload
    },

    SET_CURRENT_SLUG(state, slug: string) {
      state.currentSlug = slug
    },

    ADD_ROW(state, payload: ListRow) {
      state.shows[state.currentSlug].rows.push(payload)
    },

    DELETE_ROW(
      state,
      payload: {
        index: number
      }
    ) {
      state.shows[state.currentSlug].rows.splice(payload.index, 1)
    },

    SET_ROW_ATTR(
      state,
      payload: {
        index: number
        attr: string
        value: unknown
      }
    ) {
      // @ts-ignore
      state.shows[state.currentSlug].rows[payload.index][payload.attr] = payload.value
    },

    SET_UI_ATTR(
      state,
      payload: {
        attr: string
        value: unknown
      }
    ) {
      // @ts-ignore
      state.ui[payload.attr] = payload.value
    },

    SET_SHOW_DETAILS(state, payload: ShowDetails) {
      state.shows[state.currentSlug].meta = payload
    },

    SWAP_ROWS(
      state,
      payload: {
        from: number
        to: number
      }
    ) {
      ;[
        state.shows[state.currentSlug].rows[payload.to],
        state.shows[state.currentSlug].rows[payload.from],
      ] = [
        state.shows[state.currentSlug].rows[payload.from],
        state.shows[state.currentSlug].rows[payload.to],
      ]
    },

    ADD_SHOW(state, payload) {
      state.shows[payload.slug] = payload.showData
    },
  },

  actions: {
    checkSession({ state }) {
      return new Promise((resolve, reject) => {
        if (!storageAvailable('localStorage')) {
          state.isLoading = false
          reject(new Error('localStorage not available'))
        } else {
          const sessionKey = localStorage.getItem('program-builder-session')

          // if (sessionKey) {
          logger.log('no session, creating new one')
          localStorage.setItem('program-builder-session', generateUUID())
          state.firstTime = true
          state.isLoading = false
          // } else {
          //   console.log('session found')
          //   state.firstTime = false
          //   state.isLoading = false
          // }
          resolve()
        }
      })
    },
  },
})
