import { Store } from 'vuex'
import { State, Show } from '@/types'

export const setRowIsSelected = (
  store: Store<State>,
  index: number,
  value: boolean
): void => {
  store.commit('SET_ROW_ATTR', {
    index,
    attr: 'isSelected',
    value,
  })
}

export const demoShow: Show = {
  meta: {
    id: 'demo',
    title: 'Demo Show',
    startTime: '22:00',
    duration: 120,
    averageSongLength: 240,
  },
  rows: [
    {
      type: 'song',
      isFixed: false,
      isSpare: false,
      isSelected: false,
      duration: 324,
      bpm: 165,
      song: 'The Midnight - Days of Thunder',
    },
    {
      type: 'jingle',
      isFixed: false,
      isSpare: false,
      isSelected: false,
      duration: 5,
      text: 'RWS #1',
    },
    {
      type: 'talk',
      isFixed: false,
      isSpare: false,
      isSelected: false,
      duration: 60,
      text: 'Good Hello!',
    },
    {
      type: 'song',
      isFixed: false,
      isSpare: false,
      isSelected: false,
      duration: 254,
      bpm: 112,
      song: 'Jessie Frye w/ Timecop1983 - Faded Memory',
    },
    {
      type: 'section',
      isFixed: false,
      isSpare: false,
      isSelected: false,
      duration: 0,
      bpm: 0,
      text: 'Second half',
    },
    {
      type: 'song',
      isFixed: false,
      isSpare: false,
      isSelected: false,
      duration: 348,
      bpm: 102,
      song: 'The 1975 - Somebody Else',
    },
    {
      type: 'talk',
      isFixed: false,
      isSpare: false,
      isSelected: false,
      duration: 30,
      text: 'Welcome listeners, talk about the theme',
    },
    {
      type: 'song',
      isFixed: false,
      isSpare: false,
      isSelected: false,
      duration: 223,
      bpm: 91,
      song: 'Michael Oakley - California',
    },
    {
      type: 'song',
      isFixed: false,
      isSpare: false,
      isSelected: false,
      duration: 291,
      bpm: 116,
      song: 'Le Cassette - Digital Power',
    },
  ],
}
