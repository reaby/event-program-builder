import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import Index from './views/Index.vue'
import Build from './views/Build.vue'
import About from './views/About.vue'
import Changes from './views/Changes.vue'
import New from './views/New.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Index',
    component: Index,
  },
  {
    path: '/about/',
    name: 'About',
    component: About,
  },
  {
    path: '/changes/',
    name: 'Changes',
    component: Changes,
  },
  {
    path: '/new/',
    name: 'New',
    component: New,
  },
  {
    path: '/:slug/',
    name: 'Build',
    component: Build,
  },
]

export const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  strict: true,
  routes,
})
