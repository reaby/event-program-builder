/* eslint-disable */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  presets: [require('@slipmatio/tailwind-config')],
  theme: {
    extend: {
      colors: {
        rose: {
          50: '#fff1f2',
          100: '#ffe4e6',
          200: '#fecdd3',
          300: '#fda4af',
          400: '#fb7185',
          500: '#f43f5e',
          600: '#e11d48',
          700: '#be123c',
          800: '#9f1239',
          900: '#881337',
        },
      },
      fontFamily: {
        source: ['Source Sans Pro', 'sans-serif'],
      },
    },
  },
  variants: {
    // The 'active' variant will be generated in addition to the defaults
    extend: {
      backgroundColor: ['active'],
    },
  },
}
