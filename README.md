![code-coverage](https://gitlab.com/slipmatio/event-program-builder/badges/trunk/coverage.svg)

# Opinionated Vue CLI Template

- Vue 3
- Vue router
- Vuex
- TypeScript
- Tailwind CSS
- Cypress.io e2e tests (w/ coverage)
- GitLab CI
  - Run tests on push
- Github Actions
  - Run tests on push
  - Run Dependabot weekly

## Project setup

```
yarn install
```

### Starts development server

```
yarn dev
```

### Compiles and minifies for production

```
yarn build
```

### Run your end-to-end tests

```
yarn test:e2e
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
