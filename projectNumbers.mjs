import globby from 'globby'
import util from 'util'
import { exec as ex } from 'child_process'

const exec = util.promisify(ex)

async function traverse() {
  let totalLines = 0
  let lines = 0
  let jsFiles = 0
  let tsFiles = 0
  let vueFiles = 0
  let vueJsFiles = 0
  let vueTsFiles = 0

  const paths = await globby([
    'src/**/*.ts',
    'src/**/*.js',
    'src/**/*.vue',
    '!src/components/common/icons/*.vue',
  ])
  console.log(`Traversing ${paths.length} files`)

  for (const path of paths) {
    const { error, stdout } = await exec(`wc -l ${path}`)

    if (error) {
      console.error(`exec error: ${error}`)
      return
    }
    if (path.endsWith('.ts')) {
      tsFiles += 1
    } else if (path.endsWith('.js')) {
      jsFiles += 1
    } else {
      vueFiles += 1
      try {
        const { s2 } = await exec(`grep -lc 'lang="ts"' ${path}`)
        if (s2 === '0') {
          vueJsFiles += 1
          console.log(`js: ${path}`)
        } else {
          vueTsFiles += 1
        }
      } catch (err2) {
        try {
          await exec(`grep -lc '<script' ${path}`)
          // fType = 'js'
          vueJsFiles += 1
          console.log(`js [#${vueJsFiles}]: ${path}`)
        } catch (err4) {
          // fType = 'ts'
          vueTsFiles += 1
        }
      }
    }

    lines = parseInt(stdout.trim().split(' ')[0])
    totalLines += lines
    // console.log(`[${fType}] ${lines} : ${path}`)
  }

  console.log('Totals:')
  console.log(`  - ${totalLines} lines of code`)
  console.log(`  - ${jsFiles} JavaScript files`)
  console.log(`  - ${tsFiles} TypeScript files`)
  console.log(`  - ${vueFiles} Vue files`)
  console.log(`    - ${vueJsFiles} JS files`)
  console.log(`    - ${vueTsFiles} TS files`)
}

traverse()
