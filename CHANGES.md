# Changelog

### 0.3.0 - 2020-11-13

- New: show details can now be edited.
- Enhancement: polished the UI in many places.
- Under the hood: major refactor: added support for multiple programs.
- Under the hood: updated project dependencies.

### 0.2.0 - 2020-10-28

- New: tap tempo. Select any song and click tap tempo button in
  tempo for few times to add BPM to a song.
- New: added calculations for scripted and unscripted minutes +
  estimation of number of needed songs (based on given song
  average length)
- New: rows can now be selected and deleted
- New: added new section row type
- Enhancement: lots of UI tweaks

### 0.1.0 - 2020-10-25

- First version. Basic demo with automatic starting time, rearranging w/
  drag&drop and adding of rows
