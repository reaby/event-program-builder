// https://docs.cypress.io/api/introduction/api.html

describe('Homepage', () => {
  it('Shows correct text', () => {
    cy.viewport('macbook-13')
    cy.visit('/')

    cy.location().should(loc => {
      expect(loc.pathname).to.eq('/demoshow/')
    })

    cy.contains('#main-title', 'Demo Show')
  })
})
